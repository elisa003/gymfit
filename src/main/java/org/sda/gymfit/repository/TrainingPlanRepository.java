package org.sda.gymfit.repository;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.sda.gymfit.entities.Customer;
import org.sda.gymfit.entities.TrainingPlan;

import java.util.Optional;

public class TrainingPlanRepository {
    SessionFactory sessionFactory;

    public TrainingPlanRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public Optional<TrainingPlan> getByDescription(String description) {
        Transaction tx = null;

        try (Session session = sessionFactory.getCurrentSession()) {
            tx = session.beginTransaction();
            Optional<TrainingPlan> result = session
                    .createQuery("from TrainingPlan c where c.description = :description ")
                    .setParameter("description", description)
                    .stream().findFirst();
            tx.commit();
            return result;
        }
        catch (Exception ex) {
            ex.printStackTrace();
            if(tx != null)
                tx.rollback();
        }

        return Optional.empty();
    }

    public TrainingPlan create(TrainingPlan item) {
        Transaction tx = null;
        try (Session session = sessionFactory.getCurrentSession()) {
            tx = session.beginTransaction();
            session.persist(item);
            tx.commit();
            return item;
        }
        catch (Exception ex) {
            ex.printStackTrace();
            if(tx != null)
                tx.rollback();
        }

        return item;
    }
}
