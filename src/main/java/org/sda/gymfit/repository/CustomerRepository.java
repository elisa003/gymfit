package org.sda.gymfit.repository;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.sda.gymfit.entities.Customer;

import java.util.Optional;

public class CustomerRepository {

    private SessionFactory factory;

    public CustomerRepository(SessionFactory factory) {
        this.factory = factory;
    }

    public Optional<Customer> getById(long id) {
        Transaction tx = null;

        try (Session session = factory.getCurrentSession()) {
            tx = session.beginTransaction();
            Optional<Customer> result = session
                    .createQuery("from Customer c where c.id = :id ")
                    .setParameter("id", id)
                    .stream().findFirst();
            tx.commit();
            return result;
        }
        catch (Exception ex) {
            ex.printStackTrace();
            if(tx != null)
                tx.rollback();
        }

        return Optional.empty();
    }

    public Customer create(Customer customer) {
        Transaction tx = null;

        try (Session session = factory.getCurrentSession()) {
            tx = session.beginTransaction();
            session.persist(customer);
            tx.commit();
            return customer;
        }
        catch (Exception ex) {
            ex.printStackTrace();
            if(tx != null)
                tx.rollback();
        }

        return customer;
    }
}
