package org.sda.gymfit.entities;


import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Table(name="gf_customer")
@Getter @Setter
public class Customer {
@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="customer_id")
    protected long id;
@Column(name="first_name")
    protected String firstName;
@Column(name="last_name")
    protected String lastName;
@Column(name="registration_date")
    protected LocalDate registrationDate;

    @ManyToOne
    @JoinColumn(name = "membership_id")
    private Membership membership;
}

