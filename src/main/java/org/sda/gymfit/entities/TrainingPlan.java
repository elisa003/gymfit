package org.sda.gymfit.entities;

import jakarta.persistence.*;
import lombok.*;

import java.util.ArrayList;
import java.util.List;


@Getter
@Setter
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name="gf_training_plan")
public class TrainingPlan {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected long id;
    @Column(name="description")
    protected String description;
    @Column (name="price")
    protected  long price;

    @Column
    protected int durability;

    @OneToMany(mappedBy = "trainingPlan")
    protected List<Membership> membership = new ArrayList<>();

}
