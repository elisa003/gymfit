package org.sda.gymfit.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="gf_membership")
@Getter
@Setter

public class Membership  {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected long id;

    @Column(name="start_date")
    protected LocalDate startDate;
    @Column(name="end_date")
    protected LocalDate endDate;

    @OneToMany (mappedBy = "membership")
    protected List<Customer> customer = new ArrayList<>();

    @Column(name = "training_plan_id")
    protected long trainingPlanId;

    @ManyToOne
    @JoinColumn(name = "training_plan_id", insertable = false, updatable = false)
    private TrainingPlan trainingPlan;
}
