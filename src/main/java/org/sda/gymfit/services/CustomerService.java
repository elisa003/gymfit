package org.sda.gymfit.services;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.sda.gymfit.entities.Customer;
import org.sda.gymfit.repository.CustomerRepository;
import org.sda.gymfit.repository.MembershipRepository;
import org.sda.gymfit.repository.TrainingPlanRepository;

import java.time.LocalDate;
import java.util.*;

@RequiredArgsConstructor
public class CustomerService {
    @NonNull private CustomerRepository customerRepository;
    @NonNull private MembershipRepository membershipRepository;
    @NonNull private TrainingPlanRepository trainingPlanRepository;
    private Scanner scanner = new Scanner(System.in);

    private static Map<String, Customer> registrationMap = new HashMap<>();

    public void showMenu() {
        System.out.println("Hibernate started");

        int choice;

        do {
            System.out.println("Welcome to GymFit System");
            System.out.println("1. Check registration status");
            System.out.println("2. Register a customer");
            System.out.println("3. Activate training plan");
            System.out.println("4. List registered customers");
            System.out.println("5. Exit");
            System.out.print("Enter your choice: ");
            choice = scanner.nextInt();

            switch (choice) {
                case 1:
                    checkRegistrationStatus();
                    break;
                case 2:
                    registerCustomer();
                    break;
                case 3:
                    modifyData();
                    break;
                case 4:
                    listRegisteredCustomers();
                    break;
                case 5:
                    System.out.println("Exiting program... Goodbye!");
                    break;
                default:
                    System.out.println("Invalid choice. Please try again.");
            }
        } while (choice != 5);
    }


    private  void registerCustomer() {
        try {
            System.out.print("Enter first name: ");
            String firstName = scanner.next();
            System.out.print("Enter  last name: ");
            String lastName = scanner.next();

            Customer customer = new Customer();
            customer.setFirstName(firstName);
            customer.setLastName(lastName);
            customer.setRegistrationDate(LocalDate.now());
            customer = customerRepository.create(customer);

            System.out.println("Customer with account " + customer.getId() + ", successfully registered.");
        }
        catch (Exception ex) {
            System.out.printf("Error during customer registration! %s\n", ex.getMessage());
        }
    }

    private void checkRegistrationStatus() {
        System.out.print("Enter customer ID: ");
        long id = scanner.nextLong();
        Optional<Customer> dbResult =  customerRepository.getById(id);
        if (dbResult.isPresent()) {
            Customer cs  = dbResult.get();
            System.out.printf("=========== Customer %s ============\n", cs.getId());
            System.out.printf("Name: %s\n", cs.getFirstName());
            System.out.printf("Last Name: %s\n", cs.getLastName());
            System.out.printf("Registration date: %s\n", cs.getRegistrationDate());
            System.out.printf("=====================================\n");
        }
        else {
            System.out.printf("=====================================\n");
            System.out.println("Customer is not registered");
            System.out.printf("=====================================\n");
        }
    }

    private void modifyData() {
        System.out.print("Enter customer ID: ");
        long id = scanner.nextLong();
        //boolean isRegistered = registrationMap.containsKey(id);
        Optional<Customer> dbResult = customerRepository.getById(id);
        if (dbResult.isPresent()) {
            Customer customer = dbResult.get();
            System.out.println("Customer " + id + " is registered.");
            System.out.println("First Name: " + customer.getFirstName());
            System.out.println("Last Name: " + customer.getLastName());

            // Check if user has an active training plan
            // If there is one, print details
            // else show the training plan list and ask user to choose one by id
            // Save entry in membership
            // Show success message to console

        } else {
            System.out.println("Customer is not registered.");
        }
    }

    private static void listRegisteredCustomers() {
        System.out.println("Registered Customer:");
        if (registrationMap.isEmpty()) {
            System.out.println("No custmers have been registered.");
        } else {
            for (Map.Entry<String, Customer> entry : registrationMap.entrySet()) {
                String id = entry.getKey();
                Customer customer = entry.getValue();
                System.out.println("Customer ID: " + id);
                System.out.println("First Name: " + customer.getFirstName());
                System.out.println("Last Name: " + customer.getLastName());
                System.out.println("Registration Date: " + customer.getRegistrationDate());
                System.out.println("------------------------");
            }
        }
    }

}
