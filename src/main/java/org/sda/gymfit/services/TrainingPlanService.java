package org.sda.gymfit.services;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.sda.gymfit.entities.TrainingPlan;
import org.sda.gymfit.repository.TrainingPlanRepository;

import java.util.Optional;

@RequiredArgsConstructor
public class TrainingPlanService {
    @NonNull protected TrainingPlanRepository trainingPlanRepository;

    public void seedData() {
        TrainingPlan first = TrainingPlan.builder()
                .description("3 times for month")
                .price(4000)
                .durability(31)
                .build();
        Optional<TrainingPlan> dbResult = trainingPlanRepository.getByDescription("3 times for month");
        if (dbResult.isEmpty()) {
            trainingPlanRepository.create(first);
        }
        TrainingPlan second = TrainingPlan.builder()
                .description("4 times for month")
                .price(4500)
                .durability(31)
                .build();
        dbResult = trainingPlanRepository.getByDescription("4 times for month");
        if (dbResult.isEmpty()) {
            trainingPlanRepository.create(second);
        }
        TrainingPlan third = TrainingPlan.builder()
                .description("Every day")
                .price(5000)
                .durability(31)
                .build();

        dbResult = trainingPlanRepository.getByDescription("Every day");
        if (dbResult.isEmpty()) {
            trainingPlanRepository.create(third);
        }
    }
}
