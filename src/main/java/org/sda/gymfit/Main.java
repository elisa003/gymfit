package org.sda.gymfit;

import org.sda.gymfit.Utilities.DbConnection;
import org.sda.gymfit.entities.Customer;
import org.sda.gymfit.repository.CustomerRepository;
import org.sda.gymfit.repository.MembershipRepository;
import org.sda.gymfit.repository.TrainingPlanRepository;
import org.hibernate.SessionFactory;
import org.sda.gymfit.services.CustomerService;
import org.sda.gymfit.services.TrainingPlanService;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;


public class Main {


    public static void main(String[] args) {
        SessionFactory sessionFactory = DbConnection.getFactory();
        CustomerRepository customerRepository = new CustomerRepository(sessionFactory);
        MembershipRepository membershipRepository = new MembershipRepository(sessionFactory);
        TrainingPlanRepository trainingPlanRepository = new TrainingPlanRepository(sessionFactory);

        CustomerService customerService = new CustomerService(customerRepository, membershipRepository, trainingPlanRepository);
        TrainingPlanService trainingPlanService = new TrainingPlanService(trainingPlanRepository);
        trainingPlanService.seedData();
        customerService.showMenu();

    }


}

